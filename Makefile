SHELL := /bin/bash
# Install juno-pp

DESTDIR=debian/juno-pp

install-core:
	install -dm755 $(DESTDIR)/etc/systemd/system/
	install -dm755 $(DESTDIR)/usr/bin/
	install -dm755 $(DESTDIR)/etc/udev/rules.d/
	install -Dpm 0755 juno-pp $(DESTDIR)/usr/bin/juno-pp
	install -Dpm 0644 juno-pp.service $(DESTDIR)/etc/systemd/system/juno-pp.service
	install -Dpm 0644 juno-pp-suspend.service $(DESTDIR)/etc/systemd/system/juno-pp-suspend.service
	install -Dpm 0644 juno-pp.rules $(DESTDIR)/etc/udev/rules.d/juno-pp.rules

install: install-core

uninstall:
	rm -f $(DESTDIR)$(DESTDIR)/usr/bin/juno-pp
	rm -f $(DESTDIR)/etc/systemd/system/juno-pp.service
	rm -f $(DESTDIR)/etc/systemd/system/juno-pp-suspend.service
	rm -f $(DESTDIR)/etc/udev/rules.d/juno-pp.rules